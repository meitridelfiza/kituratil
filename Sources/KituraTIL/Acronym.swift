//
//  Acronym.swift
//  KituraTIL
//
//  Created by MacBook on 03/09/19.
//

import CouchDB

struct Acronym: Document {
    let _id: String?
    var _rev: String?
    var short: String
    var long: String
}
